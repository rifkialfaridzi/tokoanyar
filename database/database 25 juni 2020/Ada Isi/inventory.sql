-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2020 at 04:16 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `unit` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `rak` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `unit`, `category`, `rak`, `image`, `stock`, `harga_penjualan`, `min_stock`, `note`, `created_at`) VALUES
(30, 'SKU0001', 'Daster B.Rainbow', 23, 9, '7', '20200509_1517082.jpg', '45', '13000', '0', 'Allsize', '2020-06-25'),
(31, 'SKU0002', 'Daster Pink Aksara', 23, 9, '7', 'grosirmaharanysekarsolo_B6vCvoJgBZC2.jpg', '20', '13000', '0', 'Allsize', '2020-06-25'),
(32, 'SKU0003', 'Outer Green Motif K', 23, 13, '8', 'grosirmaharanysekarsolo_B3Wg_RDAjaZ1.jpg', '45', '60000', '0', 'Allsize', '2020-06-25'),
(33, 'SKU0004', 'Outer Noah Hitam', 23, 13, '8', '20200509_1517262.jpg', '50', '73000', '0', 'Allsize', '2020-06-25'),
(34, 'SKU0005', 'Kain Motif Ungu Horizon', 23, 10, '9', 'grosirmaharanysekarsolo_B_XcM5Pg4Yv1.jpg', '50', '20000', '0', '-', '2020-06-25'),
(35, 'SKU0006', 'Kain Motif Dayak S', 23, 10, '9', 'grosirmaharanysekarsolo_B-vaLO-AdVX1.jpg', '50', '22000', '0', '-', '2020-06-25'),
(36, 'SKU0007', 'Kebaya Hitam H.Dada', 23, 11, '10', 'grosirmaharanysekarsolo_B6LH7uZAear1.jpg', '50', '70000', '0', 'Allsize', '2020-06-25'),
(37, 'SKU0008', 'Kebaya Jawa M.K', 23, 11, '10', 'grosirmaharanysekarsolo_B6LJZ6AgvdT1.jpg', '50', '75000', '0', '-', '2020-06-25'),
(38, 'SKU0009', 'Kemeja Batik Motif Hijau B', 23, 14, '11', 'grosirmaharanysekarsolo_B4PxCr7HuBt1.jpg', '50', '50000', '0', 'Allsize', '2020-06-25'),
(39, 'SKU0010', 'Kemeja Hitam Putih Guru', 23, 14, '11', '20200429_1543311.jpg', '50', '45000', '0', 'Allsize', '2020-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `total_pembelian` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kode`, `barang`, `supplier`, `harga_pembelian`, `total_pembelian`, `status`, `jumlah`, `created_at`) VALUES
(68, 'TR438967', 30, 11, '9000', '0', 1, '50', '2020-06-25'),
(69, 'TR776973', 31, 11, '10000', '0', 1, '50', '2020-06-25'),
(70, 'TR735532', 32, 11, '50000', '0', 1, '50', '2020-06-25'),
(71, 'TR554758', 33, 11, '65000', '0', 1, '50', '2020-06-25'),
(72, 'TR344051', 34, 11, '13000', '0', 1, '50', '2020-06-25'),
(73, 'TR388406', 35, 11, '15000', '0', 1, '50', '2020-06-25'),
(74, 'TR827141', 36, 10, '55000', '0', 1, '50', '2020-06-25'),
(75, 'TR339871', 37, 10, '60000', '0', 1, '50', '2020-06-25'),
(76, 'TR779472', 38, 10, '40000', '0', 1, '50', '2020-06-25'),
(77, 'TR658895', 39, 10, '30000', '0', 1, '50', '2020-06-25'),
(78, 'TR576007', 30, 11, '9000', '0', 2, '20', '2020-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(9, 'Daster'),
(10, 'Kain'),
(11, 'Kebaya'),
(12, 'Gamis'),
(13, 'Outer'),
(14, 'Kemeja');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `bill` varchar(100) NOT NULL,
  `kembalian` varchar(100) NOT NULL,
  `pelanggan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `total_penjualan`, `bill`, `kembalian`, `pelanggan`, `created_at`) VALUES
(89, 'TRX781258-20200625', 1, '260000', '260000', '0', 'Pelanggan Terhormat', '2020-06-25 08:42:22'),
(90, 'TRX309944-20200625', 1, '390000', '390000', '0', 'Pelanggan Terhormat', '2020-06-25 09:09:57'),
(91, 'TRX816634-20200625', 1, '300000', '301000', '-1000', 'Pelanggan Terhormat', '2020-06-25 09:11:58');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `total`, `penjualan`) VALUES
(125, 30, '20', '260000', 89),
(126, 31, '30', '390000', 90),
(127, 32, '5', '300000', 91);

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama`) VALUES
(7, 'RAK0001', 'Rak 1'),
(8, 'RAK0002', 'Rak 2'),
(9, 'RAK0003', 'Rak 3'),
(10, 'RAK0004', 'Rak 4'),
(11, 'RAK0005', 'Rak 5'),
(12, 'RAK0006', 'Rak 6'),
(13, 'RAK0007', 'Rak 7'),
(14, 'RAK0008', 'Rak 8'),
(15, 'RAK0009', 'Rak 9'),
(16, 'RAK0010', 'Rak 10');

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(225) NOT NULL,
  `note` text NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refund`
--

INSERT INTO `refund` (`id`, `kode`, `barang_masuk`, `jumlah`, `supplier`, `harga_pembelian`, `note`, `user`, `created_at`) VALUES
(28, 'RTR576007-252506062020', 78, '5', 11, '9000', 'xxx', 1, '2020-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pemilik` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `note` text NOT NULL,
  `total_pembelian` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kode`, `barang_masuk`, `barang`, `user`, `supplier`, `harga_pembelian`, `harga_penjualan`, `amount`, `balance`, `note`, `total_pembelian`, `status`, `created_at`) VALUES
(202, 'STR438967-252506062020', 68, 30, 1, 11, '9000', '13000', 50, 30, 'Kondisi Ok', '450000', 0, '2020-06-25'),
(203, 'OTR319344', 68, 30, 0, 11, '9000', '13000', -20, -20, 'Barang Ok', '260000', 1, '2020-06-25'),
(204, 'STR776973-252506062020', 69, 31, 1, 11, '10000', '13000', 50, 20, 'Kondisi Ok', '500000', 0, '2020-06-25'),
(205, 'STR735532-252506062020', 70, 32, 1, 11, '50000', '60000', 50, 45, 'Kondisi Ok', '2500000', 0, '2020-06-25'),
(206, 'STR554758-252506062020', 71, 33, 1, 11, '65000', '73000', 50, 50, 'Kondisi Ok', '3250000', 0, '2020-06-25'),
(207, 'STR344051-252506062020', 72, 34, 1, 11, '13000', '20000', 50, 50, 'Kondisi Ok', '650000', 0, '2020-06-25'),
(208, 'STR388406-252506062020', 73, 35, 1, 11, '15000', '22000', 50, 50, 'Kondisi Ok', '750000', 0, '2020-06-25'),
(209, 'STR827141-252506062020', 74, 36, 1, 10, '55000', '70000', 50, 50, 'Kondisi Ok', '2750000', 0, '2020-06-25'),
(210, 'STR339871-252506062020', 75, 37, 1, 10, '60000', '75000', 50, 50, 'Kondisi Ok', '3000000', 0, '2020-06-25'),
(211, 'STR779472-252506062020', 76, 38, 1, 10, '40000', '50000', 50, 50, 'Kondisi Ok', '2000000', 0, '2020-06-25'),
(212, 'STR658895-252506062020', 77, 39, 1, 10, '30000', '45000', 50, 50, 'Kondisi Ok', '1500000', 0, '2020-06-25'),
(213, 'STR576007-252506062020', 78, 30, 1, 11, '9000', '13000', 15, 15, 'Kondisi Ok', '135000', 0, '2020-06-25'),
(214, 'OTR965369', 69, 31, 0, 11, '10000', '13000', -30, -30, 'Barang Ok', '390000', 1, '2020-06-25'),
(215, 'OTR547951', 70, 32, 0, 11, '50000', '60000', -5, -5, 'Barang Ok', '300000', 1, '2020-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(10, 'SP931467', 'Pusat Batik Surakarta', 'Jl. Slamet Riyadi', '085432234112', '2020-06-01'),
(11, 'SP905567', 'Industri Supra Txt', 'Jl. Kemangi Laweyan', '087766545611', '2020-06-01');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`) VALUES
(23, 'PCS'),
(24, 'Pack'),
(25, 'Lusin'),
(26, 'Seri');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `created_at`) VALUES
(1, 'admingudang', 'Ibu Siti GMH', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '2020-03-21'),
(2, 'adminkasir', 'Kasir Maharany', 'adminkasir@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '2020-03-21'),
(3, 'adminowner', 'Ibu Maliyana', 'adminowner@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '2020-03-21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `category` (`category`),
  ADD KEY `rak` (`rak`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuk` (`barang_masuk`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `user` (`user`),
  ADD KEY `barang_masuk``` (`barang_masuk`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
