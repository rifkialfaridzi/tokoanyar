<section class="section">
	<div class="section-header">
		<h1>Halaman Stock Produk</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Stock Produk</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>Gambar</th>
										<th>Nama</th>
										<th>Kode</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Min Stock</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Produk Masuk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#unit_tabel').DataTable({
      // Load data for the table's content from an Ajax source
      "order": [[ 3, "asc" ]],
			"ajax": {
				"url": '<?php echo site_url('barang/product_bystock'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <img alt="image" src="<?php echo site_url('assets/uploads/'); ?>'+ row.image + '" class="rounded-circle" width="35" data-toggle="tooltip" title="'+ row.nama + '">';
					}
				},
				{
					"data": "nama"
				},
        {
					"data": "kode"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-success" data-toggle="tooltip" title="Harga Jual">Rp.'+ row.harga_penjualan + '</div>';
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
            //console.log(row.stock + row.min_stock);
						if (parseInt(row.stock) > parseInt(row.min_stock)) {
							return ' <div class="badge badge-info" data-toggle="tooltip" title="Stock Yang Tersedia">'+ row.stock + '</div>';
						}else{
							return ' <div class="badge badge-danger" data-toggle="tooltip" title="Stock Yang Tersedia">'+ row.stock + '</div>';
						}
						
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-primary" data-toggle="tooltip" title="Minimal Stock">'+ row.min_stock + '</div>';
					}
				},
				{ 
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("stock/detail/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle" data-toggle="tooltip" title="Lihat Detail"></i></a> <a target="_blank" href="<?php echo site_url("barang/print_qr/") ?>' + row.id + '" class="btn btn-icon btn-danger"><i class="fas fa-qrcode" data-toggle="tooltip" title="Cetak QRcode"></i></a>';
						
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("pembelian/masuk/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});

	// Daterangepicker
	if(jQuery().daterangepicker) {
    if($(".datepicker").length) {
      $('.datepicker').daterangepicker({
        locale: {format: 'YYYY-MM-DD'},
        singleDatePicker: true,
      });
    }
    if($(".datetimepicker").length) {
      $('.datetimepicker').daterangepicker({
        locale: {format: 'YYYY-MM-DD hh:mm'},
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
      });
    }
    if($(".daterange").length) {
      $('.daterange').daterangepicker({
        locale: {format: 'YYYY-MM-DD'},
        drops: 'down',
        opens: 'right'
      });
    }
  }
</script>