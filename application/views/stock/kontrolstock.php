<section class="section">
    <div class="section-header">
        <h1>Halaman Kontrol Stock Produk</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Kontrol Stock</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Pilih Tanggal</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-calendar"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control daterange-cus">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <a id="cetak" href="#" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="unit_tabel" class="table table-striped">
                                <thead>
                                    <tr>

                                        <th>Gambar</th>
                                        <th>Nama</th>
                                        <th>Kategori</th>
                                        <th>Rak</th>
                                        <th>Total Masuk</th>
                                        <th>Total Keluar</th>
                                        <th>Tersedia</th>
                                        <th>Harga Jual</th>
                                        <th>Harga Grosir</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Produk Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin ?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $("#notif_stock").hide();
    var save_method; //for save method string
    var table;

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
   
    var firstdatestring = firstDay.getFullYear() + "-" + (("0" + (firstDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + firstDay.getDate()).slice(-2);
    var lastdatestring = lastDay.getFullYear() + "-" + (("0" + (lastDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + lastDay.getDate()).slice(-2);

  
    $(document).ready(function() {

        var currMonth = new Date().getMonth() + 1;
        //datatables
        table = $('#unit_tabel').DataTable({
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('admin/kontrol_stock_range_json/'); ?>" + firstdatestring + "/" + lastdatestring,
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columns": [{
                    "data": null,
                    "render": function(data, type, row) {
                        return ' <img alt="image" src="<?php echo site_url('assets/uploads/'); ?>' + row.image + '" class="rounded-circle" width="35" data-toggle="tooltip" title="' + row.nama + '">';
                    }
                },
                {
                    "data": "nama"
                },
                {
                    "data": "category_name"
                },
                {
                    "data": "rak_name"
                },
                {
                    "data": "totalMasuk"
                },
                {
                    "data": "totalKeluar"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {

                        if (parseInt(row.totalAvailable) > parseInt(row.min_stock)) {
                            return ' <div class="badge badge-info" data-toggle="tooltip" title="Stock Yang Tersedia">' + row.totalAvailable + '</div>';
                        } else {
                            return ' <div class="badge badge-danger" data-toggle="tooltip" title="Stock Yang Tersedia">' + row.totalAvailable + '</div>';
                        }
                    }
                },
                {
                    "data": "harga_penjualan"
                },
                {
                    "data": "harga_grosir"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<a target="_blank" href="<?php echo base_url('kontrol/stock/') ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle" data-toggle="tooltip" title="Lihat Detail"></i></a>';

                    }
                }

            ],

        });

        var getFirstDate = firstdatestring;
        var getLastDate = lastdatestring;

        $("#cetak").click(function() {
            window.open("<?php echo site_url('admin/print_kontrol_stock_byrange/'); ?>"+ firstdatestring + "/" + lastdatestring);
            // console.log(getFirstDate);
            // console.log(getLastDate);
        });
     
        $('.daterange-cus').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                drops: 'down',
                opens: 'right',
            },
            function(start, end) {
                console.log("Callback has been called!" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD'));

                table.ajax.url("<?php echo site_url('admin/kontrol_stock_range_json/'); ?>" + start.format('YYYY-MM-DD') + "/" + end.format('YYYY-MM-DD')).load();

                getFirstDate = start.format('YYYY-MM-DD');
                getLastDate = end.format('YYYY-MM-DD');

            }).val(firstdatestring+"-"+lastdatestring);

    });
</script>