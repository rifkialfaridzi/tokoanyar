<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Rak extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 3) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Rak_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
    }
    
    public function generate_code()
	{
		$query = "SELECT max(kode) as maxKode FROM rak";
		$max_code = $this->db->query($query)->row()->maxKode;
		$noUrut = (int) substr($max_code, 4, 4);
		$noUrut++;
		$char = "RAK";
		$kodeBarang = $char . sprintf("%04s", $noUrut);
		return $kodeBarang;
		
	}	

	public function index()
	{
		$data['main_content'] = 'rak/main';
		$data['page_title'] = 'Halaman Rak';
        
        $data['kode'] = $this->generate_code();
        $this->load->view('template',$data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$Rak =  $this->Rak_model->json();

		$data['draw'] = 0;
        $data['recordsTotal'] = $Rak == null ? [] : count($Rak);
        $data['recordsFiltered'] = $Rak == null ? [] : count($Rak);
        $data['data'] = $Rak == null ? [] : $Rak;
		
        echo json_encode($data);
	}

	
	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
			redirect(site_url('master/rak'));
		} else {
			$data = array(
				'kode' => $this->input->post('kode', TRUE),
				'nama' => $this->input->post('nama', TRUE),
				'deskripsi' => $this->input->post('deskripsi', TRUE),
			);

			$this->Rak_model->insert($data);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('master/rak'));
		}
	}

	public function edit($id)
	{
		$row = $this->Rak_model->get_by_id($id);

		if ($row) {
			$data = array(
				'id' => set_value('id', $row->id),
				'kode' => set_value('kode', $row->kode),
				'nama' => set_value('nama', $row->nama),
				'deskripsi' => set_value('deskripsi', $row->deskripsi),
				'main_content' => 'rak/update',
                'page_title' => 'Edit Rak'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('master/rak'));
		}
	}

	public function update_action($id)
	{
		$rak = $this->Rak_model->get_by_id($id);
		$is_unique_name = $this->input->post('nama', TRUE) != $rak->nama ? '|is_unique[rak.nama]' : '';

		$this->form_validation->set_rules('nama', 'Nama', 'required'.$is_unique_name);//|edit_unique[barang.nama.' . $id . ']

		if ($this->form_validation->run() == FALSE) {
			 $this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
            redirect(site_url('master/rak'));
		} else {
			$data = array(
				'nama' => $this->input->post('nama', TRUE),
				'deskripsi' => $this->input->post('deskripsi', TRUE),
			);

			$this->Rak_model->update($id, $data);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('master/rak'));
		}
	}

	public function delete($id)
	{
		$row = $this->Rak_model->get_by_id($id);

		if ($row) {
			$this->Rak_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('master/rak'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('master/rak'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('nama', 'nama', 'required|is_unique[rak.nama]');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel()
	{
		$this->load->helper('exportexcel');
		$namaFile = "category.xls";
		$judul = "category";
		$tablehead = 0;
		$tablebody = 1;
		$nourut = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=" . $namaFile . "");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Nama");

		foreach ($this->Category_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->nama);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
