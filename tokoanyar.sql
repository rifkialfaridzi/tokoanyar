-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2022 at 03:50 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokoanyar`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `unit` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `rak` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `harga_grosir` varchar(225) NOT NULL,
  `min_grosir` varchar(225) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `unit`, `category`, `rak`, `image`, `stock`, `harga_penjualan`, `harga_grosir`, `min_grosir`, `min_stock`, `note`, `created_at`) VALUES
(25, 'SKU0001', 'RAKET NYAMUK OKE', 23, 13, '13', '5a15cbd6-b470-4932-b7cc-798ca737b592.jpg', '50', '72000', '70000', '100', '0', 'RAKET NYAMUK OKE', '2022-06-19'),
(26, 'SKU0002', 'KENMASTER STOP KONTAK', 23, 13, '12', '5b5863a6-8be3-4954-b888-e0d6e436c45c.jpg', '100', '27000', '25000', '100', '0', 'KENMASTER STOP KONTAK', '2022-06-19'),
(27, 'SKU0003', 'KARPET ANAK', 23, 14, '12', '6d869676-0848-4350-bae3-b03425e1e5ff.jpg', '30', '52000', '50000', '99', '0', 'KARPET ANAK', '2022-06-19'),
(28, 'SKU0004', 'KARPET LEBAR', 23, 14, '13', '18138709_33ae3dc3-89c2-40fb-9bc0-2886793a4065_1280_1280.jpeg', '30', '72000', '70000', '100', '0', 'KARPET LEBAR', '2022-06-19'),
(29, 'SKU0005', 'KOMPOR GAS', 23, 16, '13', '36eba965-486b-4fa1-9cc4-a71d525d906a.jpg', '20', '320000', '300000', '100', '0', 'KOMPOR GAS', '2022-06-19'),
(30, 'SKU0006', 'SET MANGKOK SAKURA', 23, 16, '13', '58b8cf9d-381b-465a-9a0e-8574cc56af5c.jpg', '50', '32000', '30000', '100', '0', 'SET MANGKOK SAKURA', '2022-06-19'),
(31, 'SKU0007', 'SET CANGKIR KRAMIK', 23, 16, '12', '76ec2023-a2e6-44c0-9a7c-b52e4679cf6d.jpg', '69', '32000', '30000', '100', '0', 'SET CANGKIR KRAMIK', '2022-06-19'),
(32, 'SKU0008', 'SIKAT SERBAGUNA', 23, 15, '13', '323a2ad8-4de8-4e41-9eaf-d51f1c26386e.jpg', '53', '22000', '20000', '100', '0', 'SIKAT SERBAGUNA', '2022-06-19'),
(33, 'SKU0009', 'SAPU SERBA GUNA', 23, 15, '13', '7475ed07-c900-4f1a-bca6-ecc38595535c.jpg', '50', '32000', '30000', '100', '0', 'SAPU SERBA GUNA', '2022-06-19');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `total_pembelian` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kode`, `barang`, `supplier`, `harga_pembelian`, `total_pembelian`, `status`, `jumlah`, `created_at`) VALUES
(76, 'TR170562', 33, 15, '20000', '0', 1, '50', '2022-06-19'),
(77, 'TR257439', 32, 15, '15000', '0', 1, '55', '2022-06-19'),
(78, 'TR200303', 31, 14, '23000', '0', 1, '70', '2022-06-19'),
(79, 'TR401693', 29, 15, '220000', '0', 1, '20', '2022-06-19'),
(80, 'TR296460', 28, 14, '60000', '0', 1, '30', '2022-06-19'),
(81, 'TR668707', 27, 14, '40000', '0', 1, '30', '2022-06-19'),
(82, 'TR423173', 26, 14, '15000', '0', 1, '100', '2022-06-19'),
(83, 'TR548636', 25, 15, '50000', '0', 1, '50', '2022-06-19'),
(84, 'TR119425', 30, 14, '20000', '0', 1, '50', '2022-06-19');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(13, 'PERALATAN LISTRIK'),
(14, 'KARPET'),
(15, 'PEMBERSIH'),
(16, 'ALAT MAKAN');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `bill` varchar(100) NOT NULL,
  `kembalian` varchar(100) NOT NULL,
  `pelanggan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `total_penjualan`, `bill`, `kembalian`, `pelanggan`, `created_at`) VALUES
(94, 'TRX282964-20220619', 1, '76000', '76000', '0', 'Pelanggan Terhormat', '2022-06-19 08:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `harga_jual` varchar(10) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `harga_jual`, `total`, `penjualan`) VALUES
(129, 32, '2', '22000', '44000', 94),
(130, 31, '1', '32000', '32000', 94);

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama`, `deskripsi`) VALUES
(12, 'RAK0001', 'RAK MAWAR', 'RAK MAWAR'),
(13, 'RAK0002', 'RAK MELATI', 'RAK MAWAR'),
(14, 'RAK0003', 'RAK', 'ANGGREK');

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(225) NOT NULL,
  `note` text NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pemilik` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `note` text NOT NULL,
  `total_pembelian` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kode`, `barang_masuk`, `barang`, `user`, `supplier`, `harga_pembelian`, `harga_penjualan`, `amount`, `balance`, `note`, `total_pembelian`, `status`, `created_at`) VALUES
(213, 'STR170562-191906062222', 76, 33, 1, 15, '20000', '32000', 50, 50, 'Kondisi Ok', '1000000', 0, '2022-06-19'),
(214, 'STR257439-191906062222', 77, 32, 1, 15, '15000', '22000', 55, 53, 'Kondisi Ok', '825000', 0, '2022-06-19'),
(215, 'STR200303-191906062222', 78, 31, 1, 14, '23000', '32000', 70, 69, 'Kondisi Ok', '1610000', 0, '2022-06-19'),
(216, 'STR401693-191906062222', 79, 29, 1, 15, '220000', '320000', 20, 20, 'Kondisi Ok', '4400000', 0, '2022-06-19'),
(217, 'STR296460-191906062222', 80, 28, 1, 14, '60000', '72000', 30, 30, 'Kondisi Ok', '1800000', 0, '2022-06-19'),
(218, 'STR668707-191906062222', 81, 27, 1, 14, '40000', '52000', 30, 30, 'Kondisi Ok', '1200000', 0, '2022-06-19'),
(219, 'STR423173-191906062222', 82, 26, 1, 14, '15000', '27000', 100, 100, 'Kondisi Ok', '1500000', 0, '2022-06-19'),
(220, 'STR548636-191906062222', 83, 25, 1, 15, '50000', '72000', 50, 50, 'Kondisi Ok', '2500000', 0, '2022-06-19'),
(221, 'STR119425-191906062222', 84, 30, 1, 14, '20000', '32000', 50, 50, 'Kondisi Ok', '1000000', 0, '2022-06-19'),
(222, 'OTR743826', 77, 32, 0, 15, '15000', '22000', -2, -2, 'Barang Ok', '44000', 1, '2022-06-19'),
(223, 'OTR157512', 78, 31, 0, 14, '23000', '32000', -1, -1, 'Barang Ok', '32000', 1, '2022-06-19');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(14, 'SP225356', 'PT SIDO MULYA', 'GATAK, SUKOHARJO', '081393600707', '2022-06-19'),
(15, 'SP745394', 'PT VICO GENERATION', 'BAKI, SUKOHARJO', '081373836378', '2022-06-19');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`) VALUES
(23, 'PCS');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `created_at`) VALUES
(1, 'admingudang', 'Samsudin Binn', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '2020-03-21'),
(2, 'adminkasir', 'Munaroh Somad', 'adminkasir@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '2020-03-21'),
(3, 'adminowner', 'Ibu Siska', 'adminowner@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '2020-03-21'),
(5, 'budi', 'Budiman Syah', '', '21232f297a57a5a743894a0e4a801fc3', '', 2, '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `category` (`category`),
  ADD KEY `rak` (`rak`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuk` (`barang_masuk`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `user` (`user`),
  ADD KEY `barang_masuk``` (`barang_masuk`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
